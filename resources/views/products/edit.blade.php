@extends('master')

@section('content')
{{-- {{ dd($categories) }} --}}
<div class="px-0 col-lg-12">
    <h1 class="my-4">Update Product</h1>

    <form action={{ route('products.update', $product->id) }} method="post" enctype="multipart/form-data">
        @csrf
        @method('put')

        <div class="form-group">
            <label for="name">Product Name</label>
            <input type="text" name="name" class="form-control" id="name" value="{{ $product->name }}">
        </div>
        <div class="form-group">
            <label for="description">Product Description</label>
            <textarea name="description" id="description" class="form-control">{{ $product->description }}</textarea>
        </div>
        <div class="form-group">
            <label for="price">Product Price ($)</label>
            <input type="text" name="price" class="form-control" id="price" value="{{ $product->price }}">
        </div>
        <div class="form-group">
            <label for="category_id">Product Category</label>
            <select name="category_id" id="category_id" class="form-control">
                @forelse($categories as $category)
                <option value="{{ $category->id }}" @if ($category->id == $product->category_id) selected
                    @endif>{{ $category->name }}</option>
                @empty
                <option value="0">No Category Product</option>
                @endforelse
            </select>
        </div>
        <div class="form-group">
            <label for="photo">Product Photo</label>
            @if ($product->photo)
            <br />
            <span class="px-4 py-2 mb-2 badge badge-primary">Current Photo</span>
            <br />
            <img src="{{ asset('storage/'.$product->photo) }}" alt="{{ $product->photo }}" width="100" />
            <br />
            @else
            <br />
            <span class="px-4 py-2 mb-2 badge badge-danger">No Photo Profile</span>
            @endif
            <input type="file" name="photo" id="photo" class="form-control" />
            <small class="text-danger">Kosongkan jika tidak ingin merubah photo profile</small>
        </div>
        <input type="submit" value="Update" class="px-4 py-1 btn btn-primary btn-sm" />
    </form>

</div>
@endsection
