@extends('master')

@section('content')
{{-- {{ dd($categories) }} --}}
<div class="px-0 col-lg-12">
    <h1 class="my-4">New Categories</h1>

    <form action={{ route('products.store') }} method="post" enctype="multipart/form-data">
        @csrf

        <div class="form-group">
            <label for="name">Product Name</label>
            <input type="text" name="name" class="form-control @error('name') is-invalid @enderror" id="name"
                value="{{ old('name') }}">
            @error('name')
            <div class="alert alert-danger">{{ $message }}</div>
            @enderror
        </div>
        <div class="form-group">
            <label for="description">Product Description</label>
            <textarea name="description" id="description" class="form-control">{{ old('description') }}</textarea>
        </div>
        <div class="form-group">
            <label for="price">Product Price ($)</label>
            <input type="text" name="price" class="form-control @error('price')
                is-invalid
            @enderror" id="price" value="{{ old('price') }}">
            @error('price')
            <div class="alert alert-danger">{{ $message }}</div>
            @enderror
        </div>
        <div class="form-group">
            <label for="category_id">Product Category</label>
            <select name="category_id" id="category_id" class="form-control">
                @forelse($categories as $category)
                <option value="{{ $category->id }}">{{ $category->name }}</option>
                @empty
                <option value="0">No Category Product</option>
                @endforelse
            </select>
        </div>
        <div class="form-group">
            <label for="photo">Product Photo</label>
            <input type="file" name="photo" id="photo" class="form-control-file" />
        </div>
        <input type="submit" value="Save" class="px-4 py-1 btn btn-primary btn-sm" />
    </form>

</div>
@endsection
