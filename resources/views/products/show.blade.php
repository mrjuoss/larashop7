@extends('master')

@section('content')
{{-- {{ dd($categories) }} --}}
<div class="px-0 col-lg-12">
    <h1 class="my-4">Detail Product</h1>

    <table class="table">
        <tbody>
            <tr>
                <td>Product ID</td>
                <td>:</td>
                <td>{{ $product->id }}</td>
            </tr>
            <tr>
                <td>Product Name</td>
                <td>:</td>
                <td>{{ $product->name }}</td>
            </tr>
            <tr>
                <td>Product Description</td>
                <td>:</td>
                <td>{{ $product->description }}</td>
            </tr>
            <tr>
                <td>Product Category</td>
                <td>:</td>
                <td>{{ $product->category->name }}</td>
            </tr>
            <tr>
                <td>Product Price</td>
                <td>:</td>
                <td>{{ $product->price }}</td>
            </tr>
            <tr>
                <td>Product Image</td>
                <td>:</td>
                <td><img src="{{ asset($product->photo) }}" alt="{{ $product->name}}" width="500px" /></td>
            </tr>
            <tr>
                <td>Created At</td>
                <td>:</td>
                <td>{{ $product->created_at }}</td>
            </tr>
            <tr>
                <td>Updated At</td>
                <td>:</td>
                <td>{{ $product->updated_at }}</td>
            </tr>
        </tbody>
    </table>

</div>
@endsection
