@extends('master')

@section('content')
{{-- {{ dd($categories) }} --}}
<div class="px-0 col-lg-12">
    <h1 class="my-4">Products</h1>
    <a href="{{ route('products.create')}}" class="px-4 py-1 mb-2 btn btn-primary btn-sm">New Product</a>
    <table class="table">
        <thead class=" thead-dark">
            <tr>
                <th>No</th>
                <th>Name</th>
                <th>Category</th>
                <th>Price</th>
                <th>Description</th>
                <th class="text-right">Action</th>
            </tr>
        </thead>
        <tbody>
            @forelse ($products as $product)
            <tr>
                <td>{{ $loop->iteration }}</td>
                <td>{{ $product->name }}</td>
                <td>{{ ($product->category->name) ?? 'No Category' }}</td>
                <td>${{ $product->price }}</td>
                <td>{{ $product->description }}</td>
                <td class="text-right">
                    <a href="{{ route('products.show', $product->id)}}" class="btn btn-primary btn-sm">Show</a>
                    <a href="{{ route('products.edit', $product->id)}}" class="btn btn-info btn-sm">Edit</a>
                    <form action="{{ route('products.destroy', $product->id) }}" method="post" class="d-inline">
                        @csrf
                        @method('delete')
                        <input type="submit" value="delete" class="btn btn-danger btn-sm"
                            onclick="return confirm('Are you sure you want to delete this products ?')" />
                    </form>

                </td>
            </tr>
            @empty
            <tr>
                <td colspan="3">No Categories</td>
            </tr>
            @endforelse
        </tbody>
    </table>
</div>
@endsection
