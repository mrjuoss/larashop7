@extends('master')

@section('content')
<div class="d-flex">
    <div class="px-0 col-md-3">
        <h3 class="my-4">Category</h3>
        <div class="list-group">
            @forelse($categories as $category)
            <a href="/?category_id={{ $category->id }}" class="list-group-item">{{ $category->name }}</a>
            @empty
            <p>No Category</p>
            @endforelse
        </div>
    </div>
    <div class="ml-4 col-md-9">
        <div id="carouselExampleIndicators" class="my-4 carousel slide" data-ride="carousel">
            <ol class="carousel-indicators">
                <li data-target="#carouselExampleIndicators" data-slide-to="0" class="active"></li>
                <li data-target="#carouselExampleIndicators" data-slide-to="1"></li>
                <li data-target="#carouselExampleIndicators" data-slide-to="2"></li>
            </ol>
            <div class="carousel-inner" role="listbox">
                <div class="carousel-item active">
                    <img class="d-block img-fluid" src="http://placehold.it/900x350" alt="First slide">
                </div>
                <div class="carousel-item">
                    <img class="d-block img-fluid" src="http://placehold.it/900x350" alt="Second slide">
                </div>
                <div class="carousel-item">
                    <img class="d-block img-fluid" src="http://placehold.it/900x350" alt="Third slide">
                </div>
            </div>
            <a class="carousel-control-prev" href="#carouselExampleIndicators" role="button" data-slide="prev">
                <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                <span class="sr-only">Previous</span>
            </a>
            <a class="carousel-control-next" href="#carouselExampleIndicators" role="button" data-slide="next">
                <span class="carousel-control-next-icon" aria-hidden="true"></span>
                <span class="sr-only">Next</span>
            </a>
        </div>

        <div class="row">

            @forelse ($products as $product)
            <div class="mb-4 col-lg-4 col-md-6">
                <div class="card h-100">
                    <a href="#"><img class="card-img-top" src="http://placehold.it/700x400" alt=""></a>
                    <div class="card-body">
                        <h4 class="card-title">
                            <a href="#">{{ $product->name }}</a>
                        </h4>
                        <h5>${{ $product->price }}</h5>
                        <p class="card-text">{{ $product->description }}</p>
                        <hr>
                        <small>
                            Category : {{ ($product->category->name) ?? "No Category" }}
                        </small>
                    </div>
                    <div class="card-footer">
                        <small class="text-muted">&#9733; &#9733; &#9733; &#9733; &#9734;</small>
                    </div>
                </div>
            </div>
            @empty
            <p>No Product</p>
            @endforelse

        </div>
        <!-- /.row -->
    </div>
</div>
@endsection
