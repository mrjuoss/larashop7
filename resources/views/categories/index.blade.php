@extends('master')

@section('content')
{{-- {{ dd($categories) }} --}}
<div class="px-0 col-lg-12">
    <h1 class="my-4">Categories</h1>
    <a href="{{ route('categories.create')}}" class="px-4 py-1 mb-2 btn btn-primary btn-sm">New Category</a>
    <table class="table">
        <thead class=" thead-dark">
            <tr>
                <th>No</th>
                <th>Name</th>
                <th class="text-right">Action</th>
            </tr>
        </thead>
        <tbody>
            @forelse ($categories as $category)
            <tr>
                <td>{{ $loop->iteration }}</td>
                <td>{{ $category->name }}</td>
                <td class="text-right">
                    <a href="{{ route('categories.show', $category->id)}}" class="btn btn-primary btn-sm">Show</a>
                    <a href="{{ route('categories.edit', $category->id)}}" class="btn btn-info btn-sm">Edit</a>
                    <form action="{{ route('categories.destroy', $category->id) }}" method="post" class="d-inline">
                        @csrf
                        @method('delete')
                        <input type="submit" value="delete" class="btn btn-danger btn-sm"
                            onclick="return confirm('Are you sure you want to delete this category ?')" />
                    </form>

                </td>
            </tr>
            @empty
            <tr>
                <td colspan="3">No Categories</td>
            </tr>
            @endforelse
        </tbody>
    </table>
</div>
@endsection
