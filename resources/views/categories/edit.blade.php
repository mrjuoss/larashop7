@extends('master')

@section('content')
{{-- {{ dd($categories) }} --}}
<div class="px-0 col-lg-12">
    <h1 class="my-4">Categories</h1>

    <form action={{ route('categories.update', $category->id) }} method="post">
        @csrf
        @method('put')
        <div class="form-group">
            <label for="name">Category Name</label>
            <input type="text" name="name" class="form-control" id="name" value="{{ $category->name }}">
        </div>
        <input type="submit" value="Update" class="px-4 py-1 btn btn-primary btn-sm" />
    </form>

</div>
@endsection
