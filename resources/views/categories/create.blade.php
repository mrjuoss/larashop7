@extends('master')

@section('content')
{{-- {{ dd($categories) }} --}}
<div class="px-0 col-lg-12">
    <h1 class="my-4">New Categories</h1>

    <form action={{ route('categories.store') }} method="post">
        @csrf

        <div class="form-group">
            <label for="name">Category Name</label>
            <input type="text" name="name" class="form-control" id="name" value="{{ old('name') }}">
        </div>
        <input type="submit" value="Save" class="px-4 py-1 btn btn-primary btn-sm" />
    </form>

</div>
@endsection
